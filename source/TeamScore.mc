import Toybox.Lang;

class TeamScore {
    var serveRotation;
    var currentSetPoints;
    var setsWon;

    function initialize(serveRotation, currentSetPoints, setsWon) {
        self.serveRotation = serveRotation;
        self.currentSetPoints = currentSetPoints;
        self.setsWon = setsWon;
    }

    function wonPoint(opponentScore as TeamScore) as TeamScore {
        var newCurrentSetPoints = currentSetPoints + 1;
        var newServeRotation = serveRotation == ServeRotation.OppositeTeamServe ? ServeRotation.TeamAcquiresServe : ServeRotation.TeamMaintainsServe;
        var newSetsWon = GameScore.isWinningPoint(newCurrentSetPoints, opponentScore.currentSetPoints) ? setsWon + 1 : setsWon;

        return new TeamScore(
            newServeRotation,
            newCurrentSetPoints,
            newSetsWon
        );
    }

    function lostPoint() as TeamScore {
        return new TeamScore(
            ServeRotation.OppositeTeamServe,
            currentSetPoints,
            setsWon
        );
    }

    function changeServe() as TeamScore {
        return new TeamScore(
            serveRotation == ServeRotation.OppositeTeamServe ? ServeRotation.TeamMaintainsServe : ServeRotation.OppositeTeamServe,
            currentSetPoints,
            setsWon
        );
    }

    function startNewSet(acquiresServe as Boolean) as TeamScore {
        return new TeamScore(
            acquiresServe ? ServeRotation.TeamAcquiresServe : ServeRotation.OppositeTeamServe,
            0,
            setsWon
        );
    }

    function forceStartNewSet(opponentScore as TeamScore, acquiresServe as Boolean) as TeamScore {
        var newSetsWon = currentSetPoints - opponentScore.currentSetPoints > 0 ? setsWon + 1 : setsWon;
        return new TeamScore(
            acquiresServe ? ServeRotation.TeamAcquiresServe : ServeRotation.OppositeTeamServe,
            0,
            newSetsWon
        );
    }

    function toJson() {
        return {
            "serveRotation" => serverRotationId(),
            "currentSetPoints" => currentSetPoints,
            "setsWon" => setsWon
        };
    }

    private function serverRotationId() as String {
        switch (serveRotation) {
            case ServeRotation.TeamAcquiresServe:
              return "A";
            case ServeRotation.TeamMaintainsServe:
              return "M";
            default:
              return "O";
        }
    }
}