import Toybox.Graphics;
import Toybox.WatchUi;

class MainView extends WatchUi.View {
    private var gameController;
    private var myTeamScoreView;
    private var theirTeamScoreView;

    function initialize(gameController as GameController) {
        View.initialize();
        self.gameController = gameController;
    }

    function onLayout(dc as Dc) as Void {
        setLayout(Rez.Layouts.MainLayout(dc));
        myTeamScoreView = new TeamScoreView(
            findDrawableById("myTeamCurrentSetPoints"),
            findDrawableById("myTeamSetsWon"),
            findDrawableById("myTeamServeRotationIndicator")
        );
        theirTeamScoreView = new TeamScoreView(
            findDrawableById("theirTeamCurrentSetPoints"),
            findDrawableById("theirTeamSetsWon"),
            findDrawableById("theirTeamServeRotationIndicator")
        );
    }

    function onShow() as Void {
        gameController.start(method(:onDataChange));
        refreshViews();
    }

    function onUpdate(dc as Dc) as Void {
        View.onUpdate(dc);
    }

    function onHide() as Void {
        gameController.pause();
    }

    private function refreshViews() as Void {
        myTeamScoreView.refresh(gameController.gameScore.myTeamScore);
        theirTeamScoreView.refresh(gameController.gameScore.theirTeamScore);
    }

    function onDataChange() as Void {
        refreshViews();
        WatchUi.requestUpdate();
    }
}