import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;

class MainMenuDelegate extends WatchUi.MenuInputDelegate {
    var gameController;

    function initialize(gameController as GameController) {
        self.gameController = gameController;
        MenuInputDelegate.initialize();
    }

    function onMenuItem(item as Symbol) as Void {
        switch (item) {
          case :change_serve:
            gameController.changeServe();
            break;
          case :undo_score:
            gameController.undoScore();
            break;
          case :start_new_set:
            gameController.forceStartNewSet();
            break;
          case :swap_team_sides_in_remote_display:
            gameController.swapTeamSidesInRemoteDisplay();
            break;
        }
    }
}