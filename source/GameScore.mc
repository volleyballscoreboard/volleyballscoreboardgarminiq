import Toybox.Lang;
using Toybox.Math as Math;

class GameScore {
    var currentSetFirstServeTeam;
    var myTeamScore;
    var theirTeamScore;
    var previous;

    function initialize(currentSetFirstServeTeam, myTeamScore, theirTeamScore, previous) {
        self.currentSetFirstServeTeam = currentSetFirstServeTeam;
        self.myTeamScore = myTeamScore;
        self.theirTeamScore = theirTeamScore;
        self.previous = previous;
    }

    static function initial(firstServeTeam) as GameScore {
        return new GameScore(
            firstServeTeam,
            new TeamScore(
                firstServeTeam == Team.We ? ServeRotation.TeamMaintainsServe : ServeRotation.OppositeTeamServe,
                0,
                0
            ),
            new TeamScore(
                firstServeTeam == Team.They ? ServeRotation.TeamMaintainsServe : ServeRotation.OppositeTeamServe,
                0,
                0
            ),
            null
        );
    }

    function summary() as String {
        return "" + self.myTeamScore.setsWon + "(" + self.myTeamScore.currentSetPoints + ") : " +
            self.theirTeamScore.setsWon + "(" + self.theirTeamScore.currentSetPoints + ")";
    }

    function myTeamScores() as GameScore {
        if (isEndOfSet()) {
            return startNewSet();
        }

        return new GameScore(
            currentSetFirstServeTeam,
            myTeamScore.wonPoint(theirTeamScore),
            theirTeamScore.lostPoint(),
            self
        );
    }

    function theirTeamScores() as GameScore {
        if (isEndOfSet()) {
            return startNewSet();
        }

        return new GameScore(
            currentSetFirstServeTeam,
            myTeamScore.lostPoint(),
            theirTeamScore.wonPoint(myTeamScore),
            self
        );
    }

    function changeServe() as GameScore {
        return new GameScore(
            currentSetFirstServeTeam,
            myTeamScore.changeServe(),
            theirTeamScore.changeServe(),
            previous
        );
    }

    function startNewSet() as GameScore {
        return new GameScore(
            nextSetFirstServeTeam(),
            myTeamScore.startNewSet(nextSetFirstServeTeam() == Team.We),
            theirTeamScore.startNewSet(nextSetFirstServeTeam() == Team.They),
            self
        );
    }

    function forceStartNewSet() as GameScore {
        return new GameScore(
            nextSetFirstServeTeam(),
            myTeamScore.forceStartNewSet(
                theirTeamScore,
                nextSetFirstServeTeam() == Team.We
            ),
            theirTeamScore.forceStartNewSet(
                myTeamScore,
                nextSetFirstServeTeam() == Team.They
            ),
            self
        );
    }

    private function nextSetFirstServeTeam() {
        return currentSetFirstServeTeam == Team.We ? Team.They : Team.We;
    }

    function isEndOfSet() as Boolean {
        return isWinningPoint(myTeamScore.currentSetPoints, theirTeamScore.currentSetPoints);
    }

    function isStartOfNextSet() as Boolean {
        return previous != null && self.myTeamScore.currentSetPoints == 0 && self.theirTeamScore.currentSetPoints == 0;
    }

    function toJson(remoteDisplayTeamsSwapped as Boolean) {
        var leftSideTeam = remoteDisplayTeamsSwapped ? theirTeamScore : myTeamScore;
        var rightSideTeam = remoteDisplayTeamsSwapped ? myTeamScore : theirTeamScore;
        return {
            "leftSideTeam" => leftSideTeam.toJson(),
            "rightSideTeam" => rightSideTeam.toJson()
        };
    }

    function toTransmitMessage(remoteDisplayTeamsSwapped as Boolean) {
        var leftSideTeam = remoteDisplayTeamsSwapped ? theirTeamScore : myTeamScore;
        var rightSideTeam = remoteDisplayTeamsSwapped ? myTeamScore : theirTeamScore;
        var serveIndicator = "";
        
        if (leftSideTeam.serveRotation == ServeRotation.TeamAcquiresServe) {
            serveIndicator = "1";
        } else if (leftSideTeam.serveRotation == ServeRotation.TeamMaintainsServe) {
            serveIndicator = "3";
        } else if (rightSideTeam.serveRotation == ServeRotation.TeamAcquiresServe) {
            serveIndicator = "2";
        } else if (rightSideTeam.serveRotation == ServeRotation.TeamMaintainsServe) {
            serveIndicator = "4";
        }

        return "" +
          leftSideTeam.setsWon + "|" +
          leftSideTeam.currentSetPoints + "|" +
          rightSideTeam.currentSetPoints + "|" +
          rightSideTeam.setsWon + "|" +
          serveIndicator;
    } 

    static function isWinningPoint(myTeamPoints as Number, theirTeamPoints as Number) as Boolean {
        return (myTeamPoints >= 25 || theirTeamPoints >= 25) && (myTeamPoints - theirTeamPoints).abs() >= 2;
    }
}