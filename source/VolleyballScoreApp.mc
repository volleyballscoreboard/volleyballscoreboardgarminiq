import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;

class VolleyballScoreApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize();
    }

    function onStart(state as Dictionary?) as Void {
    }

    function onStop(state as Dictionary?) as Void {
    }

    function getInitialView() as [Views] or [Views, InputDelegates] {
        return [new Rez.Menus.StartMenu(), new StartMenuDelegate()];
        
    }

}

function getApp() as VolleyballScoreApp {
    return Application.getApp() as VolleyballScoreApp;
}