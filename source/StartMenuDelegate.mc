import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;

class StartMenuDelegate extends WatchUi.MenuInputDelegate {

    function initialize() {
        MenuInputDelegate.initialize();
    }

    function onMenuItem(item as Symbol) as Void {
        if (item == :quit) {
          Toybox.WatchUi.popView(Toybox.WatchUi.SLIDE_IMMEDIATE);
          return;
        }

        var firstServeTeam = item == :myTeam ? Team.We : Team.They;
        var gameController = new GameController(firstServeTeam);
        var mainView = new MainView(gameController);
        
        var mainDelegate = new MainDelegate(gameController);
        WatchUi.pushView(mainView, mainDelegate, WatchUi.SLIDE_UP);
    }
}