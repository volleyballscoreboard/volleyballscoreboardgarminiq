class TeamScoreView {
    private var currentSetPoints;
    private var setsWon;
    private var serveRotationIndicator;

    function initialize(currentSetPoints, setsWon, serveRotationIndicator) {
        self.currentSetPoints = currentSetPoints;
        self.setsWon = setsWon;
        self.serveRotationIndicator = serveRotationIndicator;
    }

    function refresh(teamScore as TeamScore) as Void {
        currentSetPoints.setText(teamScore.currentSetPoints.toString());
        setsWon.setText(teamScore.setsWon.toString());
        serveRotationIndicator.setBitmap(serveRotationIcon(teamScore.serveRotation));
    }

    private static function serveRotationIcon(serveRotation) {
        switch (serveRotation) {
            case ServeRotation.TeamAcquiresServe:
              return Rez.Drawables.ServeAcquired;
            case ServeRotation.TeamMaintainsServe:
              return Rez.Drawables.ServeMaintained;
            default:
              return Rez.Drawables.ServeOppositeTeam;
        }
    }
}