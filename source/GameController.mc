import Toybox.Lang;
using Toybox.Attention;
using Toybox.Communications;
using Toybox.Timer;
using Toybox.ActivityRecording;
using Toybox.Sensor;

class GameController {
    var gameScore;
    var remoteDisplayTeamsSwapped = false;
    var dataChangedListener;
    var timer;
    var dataVersion = -1;
    const timerEnabled   = true;
    var session;

    function initialize(firstServeTeam) {
        gameScore = GameScore.initial(firstServeTeam);

        if (timerEnabled) {
            timer = new Timer.Timer();
        }
    }

    function start(dataChangedListener) as Void {
        self.dataChangedListener = dataChangedListener;
        if (self.session == null) {
            Sensor.setEnabledSensors( [Sensor.SENSOR_HEARTRATE] );
            self.session = ActivityRecording.createSession({:name=>"Volleyball", :sport=>Activity.SPORT_VOLLEYBALL});
            self.session.start();
        }

        onDataChange();

        // Sometimes Bluetooth LE connection is lost when distance between the watch
        // and the phone with Garmin Connect mobile app is too long.
        // In such cases relying on sending the score update only when it changes
        // leads to long delays in publishing. Thus we are publishing the score
        // every two seconds which results in much shorter delays in case of
        // connectivity issues with the phone.
        if (timerEnabled) {
            timer.start(method(:publishToPhone), 2000, true);
        }
    }

    function stop(saveActivity as Boolean) as Void {
        if (self.session != null && self.session.isRecording()) {
            self.session.stop();
            if (saveActivity) {
                self.session.save();
                System.println("Session saved " + self.session);
            } else {
                self.session.discard();
                System.println("Session discarded " + self.session);
            }
            self.session = null;
        }
        Toybox.WatchUi.popView(Toybox.WatchUi.SLIDE_IMMEDIATE);
    }

    function pause() as Void {
        if (timerEnabled) {
            timer.stop();
        }
    }

    function myTeamScores() as Void {
        gameScore = gameScore.myTeamScores();
        onDataChange();
    }

    function theirTeamScores() as Void {
        gameScore = gameScore.theirTeamScores();
        onDataChange();
    }

    function undoScore() as Void {
        if (gameScore.previous != null) {
            gameScore = gameScore.previous;
        }
        onDataChange();
    }

    function changeServe() as Void {
        gameScore = gameScore.changeServe();
        onDataChange();
    }

    function forceStartNewSet() as Void {
        gameScore = gameScore.forceStartNewSet();
        onDataChange();
    }

    function swapTeamSidesInRemoteDisplay() as Void {
        remoteDisplayTeamsSwapped = !remoteDisplayTeamsSwapped;
        onDataChange();
    }

    private function swapTeamSidesInRemoteDisplayIfNeeded() as Void {
        if (gameScore.previous != null && gameScore.previous.currentSetFirstServeTeam != gameScore.currentSetFirstServeTeam) {
            remoteDisplayTeamsSwapped = !remoteDisplayTeamsSwapped;
        }
    }

    private function onDataChange() {
        swapTeamSidesInRemoteDisplayIfNeeded();

        if (self.session != null && self.session.isRecording() && self.gameScore.isStartOfNextSet()) {
            self.session.addLap();
        }

        self.dataChangedListener.invoke();
        publishToPhone();
        Attention.vibrate([
            new Attention.VibeProfile(25, 100)
        ]);
    }

    function publishToPhone() {
        var encodedGamesScore = gameScore.toTransmitMessage(remoteDisplayTeamsSwapped);
        dataVersion = dataVersion + 1;
        var scoreMessage = encodedGamesScore + "|" + dataVersion;
        $.Toybox.Communications.transmit(scoreMessage, null, new PhoneCommunicationConnectionListener());
    }

}

class PhoneCommunicationConnectionListener extends Communications.ConnectionListener {
    function initialize() {
        Communications.ConnectionListener.initialize();
    }

    function onComplete() {
        System.println("Transmit Complete");
    }

    function onError() {
        System.println("Transmit Failed");
    }
}