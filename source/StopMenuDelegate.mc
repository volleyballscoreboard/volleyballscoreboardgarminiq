import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;

class StopMenuDelegate extends WatchUi.MenuInputDelegate {
    var gameController;

    function initialize(gameController as GameController) {
        self.gameController = gameController;
        MenuInputDelegate.initialize();
    }

    function onMenuItem(item as Symbol) as Void {
        switch (item) {
          case :saveActivityAndStop:
            gameController.stop(true);
            break;
          case :discardActivityAndStop:
            gameController.stop(false);
            break;
        }
    }
}