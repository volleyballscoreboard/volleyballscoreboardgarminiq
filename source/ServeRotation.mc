class ServeRotation {
    enum {
        TeamAcquiresServe,
        TeamMaintainsServe,
        OppositeTeamServe
    }
}