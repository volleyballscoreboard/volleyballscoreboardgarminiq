import Toybox.Lang;
import Toybox.WatchUi;

class MainDelegate extends WatchUi.InputDelegate {
    var gameController;
    
    function initialize(gameController) {
        InputDelegate.initialize();
        self.gameController = gameController;
    }

    function onKey(keyEvent) {
        // System.println("Key " + keyEvent.getKey());
        
        if (keyEvent.getKey() == KEY_MENU) {
            WatchUi.pushView(new Rez.Menus.MainMenu(), new MainMenuDelegate(gameController), WatchUi.SLIDE_UP);
            return true;
        }

        if (keyEvent.getKey() == KEY_UP) {
            gameController.myTeamScores();
            return true;
        }

        if (keyEvent.getKey() == KEY_DOWN) {
            gameController.theirTeamScores();
            return true;
        }

        if (keyEvent.getKey() == KEY_ENTER) {
            gameController.undoScore();
            return true;
        }

        if (keyEvent.getKey() == KEY_ESC) {
            WatchUi.pushView(
                new Rez.Menus.StopMenu(),
                new StopMenuDelegate(gameController),
                WatchUi.SLIDE_UP
            );
            return true;
        }

        return false;
    }

    // function onDrag(dragEvent) {
    //     return true;
    // }

    // function onFlick(flickEvent) {
    //     return true;
    // }

    // function onHold(clickEvent) {
    //     return true;
    // }

    // function onRelease(clickEvent) {
    //     return true;
    // }

    // function onSwipe(swipeEvent) {
    //     return true;
    // }

    // function onTap(clickEvent) {
    //     return true;
    // }
}